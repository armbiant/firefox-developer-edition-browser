#!/bin/sh

set -e
set -u

version=$1
lang=$2

if [ "$version" = "latest" ] ; then
    if dpkg-architecture -e amd64 ; then
	os=linux64
    elif dpkg-architecture -e i386 ; then
	os=linux
    else
	exit 17
    fi
    wget -O firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=$os&lang=$lang"
else
    if dpkg-architecture -e amd64 ; then
	os=linux-x86_64
    elif dpkg-architecture -e i386 ; then
	os=linux-i686
    else
	exit 17
    fi
    wget -O firefox.tar.bz2 "https://download-installer.cdn.mozilla.net/pub/firefox/releases/$version/$os/$lang/firefox-$version.tar.bz2"
fi

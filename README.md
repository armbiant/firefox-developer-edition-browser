# How to build the package

1. Generate the files:

    python3 genpackage.py generate

2. Build the kaboxer application:

    kaboxer build

3. Build the Debian package:

    dpkg-buildpackage -us -uc

4. Inspect the package produced:

    dpkg -c ../firefox-developer-edition-*-kbx_*_amd64.deb
